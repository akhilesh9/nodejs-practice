const express = require('express');

const PORT = process.env.PORT || 5000;

const app = express();

// Initialize middleware
app.use(express.json({ extended: false }));

app.get('/', (req, res) => res.send('Api running'));

// Define routes
app.use('/verifyAadhaarPanLink', require('./routes/verify'));

app.listen(PORT, () => console.log(`Server started on port ${PORT} `));

module.exports = app;
