const express = require('express');
const router = express.Router();
const puppeteer = require('puppeteer');
const URL = "https://www1.incometaxindiaefiling.gov.in/e-FilingGS/Services/AadhaarPreloginStatus.html"

const aadharPanLinkStatusHelper = async (aadhaarNumber, panNumber) => {
    const browser = await puppeteer.launch({ headless: true, userDataDir: './data', });
    const page = await browser.newPage();
    await page.setRequestInterception(true);

    //To block loading any assest like image. This will load only the HTML content
    page.on('request', (request) => {
        if (request.resourceType() === 'document') {
            request.continue();
        } else {
            request.abort();
        }
    });

    await page.goto(
        URL,
        { waitUntil: 'load' }
    );
    await page.evaluate(
        (aadhaarNumber, panNumber) => {
            const panBox = document.querySelector('#ViewAadhaarLinkStatus_panNo');
            panBox.value = panNumber;
            const aadhaarBox = document.querySelector('#aadhaarNo');
            aadhaarBox.value = aadhaarNumber;
            const submit = document.querySelector('#aadhaarbtn');
            submit.click();
        },
        aadhaarNumber,
        panNumber
    );
    try {
        const reply = await page.waitForSelector('.success , .error');
        const divs = await page.$$(
            '.' + reply._remoteObject.description.split('.')[1]
        );
        let text = [];
        for (let i = 0; i < divs.length; i++) {
            let t = await (await divs[i].getProperty('textContent')).jsonValue();
            t = t.replace(/[\n\t]+/g, '');
            text.push(t);
        }
        browser.close();
        return text;
    } catch (error) {
        browser.close();
        return 'Problem in response received from the website';
    }
};

router.post('/', async (req, res) => {
    const { aadhaarNumber, panNumber } = req.body;
    try {
        const reply = await aadharPanLinkStatusHelper(aadhaarNumber, panNumber);
        if (
            reply.length === 1 &&
            reply[0].startsWith('Your PAN is linked to Aadhaar Number')
        ) {
            return res.status(200).send(reply);
        }
        else {
            return res.status(400).send(reply);
        }
    } catch (err) {
        return res.status(500).json({ msg: 'Internal Server Error' });
    }
});
module.exports = router;
