const express = require('express');
const router = express.Router();
var request = require('request-promise');
var cheerio = require('cheerio');
const URL = "https://www1.incometaxindiaefiling.gov.in/e-FilingGS/Services/ViewAadhaarLinkStatus.html"

const aadharPanLinkStatusHelper = async (aadhaarNumber, panNumber) => {
    const options = {
        method: 'POST',
        "rejectUnauthorized": false,
        url: URL,
        formData: {
            panNo: panNumber,
            aadhaarNo: aadhaarNumber
        }
    }
    try {
        const response = await request(options)
        let $ = cheerio.load(response);
        let text = []
        $('.success').each((index, element) => {
            let t = $(element).text()
            t = t.replace(/[\n\t]+/g, '');
            text.push(t)
        })
        //If success message is not returned, this will get the errors
        if (text.length === 0) {
            $('.error').each((index, element) => {
                let t = $(element).text()
                t = t.replace(/[\n\t]+/g, '');
                text.push(t)
            })
        }
        // If error message is also not found
        if (text.length === 0)
            text.push("Problem with website's response")
        return text
    } catch (error) {
        console.log("Error")
    }
};

router.post('/', async (req, res) => {
    const { aadhaarNumber, panNumber } = req.body
    try {
        const reply = await aadharPanLinkStatusHelper(aadhaarNumber, panNumber)
        return res.status(200).send(reply)
    } catch (err) {
        return res.status(500).json({ msg: 'Internal Server Error' });
    }
});

module.exports = router;