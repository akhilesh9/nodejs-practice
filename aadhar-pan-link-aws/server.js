const express = require('express')
const app = express()
var request = require('request-promise');


app.use(express.json({ extended: false }));

app.get('/', (req, res) => {
    res.status(200).json({ msg: 'Connection complete' })
})

app.use('/verify', require('./routes/verify'));

app.post('/checkIp', async (req, res) => {
    const options = {
        method: 'POST',
        "rejectUnauthorized": false,
        url: "http://ubuntu@13.235.135.65/checkIfIpChanges",
    }
    try {
        const response = await request(options)
        return res.send(response)
    } catch (error) {
        return res.send(error)
    }
})

// app.post('/checkIpLocally', async (req, res) => {
//     const options = {
//         method: 'GET',
//         "rejectUnauthorized": false,
//         url: "http://127.0.0.1:5000",
//     }
//     try {
//         const response = await request(options)
//         const time = new Date()
//         console.log(req.ip + " : " + time)
//         return res.send(response)
//     } catch (error) {
//         return res.send(error)
//     }
// })

module.exports = app