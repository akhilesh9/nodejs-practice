const express = require('express');
const router = express.Router();

router.get("/pdf2pic", async (req, res) => {
    const startTime = new Date()
    const PDF2Pic = require("pdf2pic");
    const pdf2pic = new PDF2Pic({
        density: 100,           // output pixels per inch
        savename: "pan-bw",   // output file name
        savedir: "./images/pdf2pic",    // output file location
        format: "png",          // output file format
        size: "600x600"         // output size in pixels
    });

    try {
        pdf2pic.convertBulk("pan-bw.pdf", -1).then((resolve) => {
            console.log("image converter successfully!");
            return resolve;
        });
    } catch (error) {
        console.log(error)
    }
    const responseTime = startTime - new Date()
    return res.send("Response time " + responseTime)
})

module.exports = router